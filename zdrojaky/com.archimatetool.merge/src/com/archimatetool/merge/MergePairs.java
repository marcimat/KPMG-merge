package com.archimatetool.merge;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import com.archimatetool.model.IProperty;
import com.archimatetool.model.impl.ArchimateConcept;

public class MergePairs {
	ArchimateConcept master, lesser;
	EList<IProperty> master_p, lesser_p;
	
	public MergePairs(ArchimateConcept master, ArchimateConcept lesser){
		this(master, lesser, null, null);
	}
	//todo mozno prerobit spat do poly
	public MergePairs(ArchimateConcept master, ArchimateConcept lesser, EList<IProperty> master_p, EList<IProperty> lesser_p){
		this.master = master; 
		this.lesser = lesser;
		if(master_p != null && lesser_p != null){
			this.master_p = new BasicEList<IProperty>();
			for(Object obj : master_p){
				this.master_p.add((IProperty) obj);
			}
			this.lesser_p = new BasicEList<IProperty>();
			for(Object obj : lesser_p){
				this.lesser_p.add((IProperty) obj);
			}
		}
		
	}
}
