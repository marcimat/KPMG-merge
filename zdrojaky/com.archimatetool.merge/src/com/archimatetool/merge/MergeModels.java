/*
** TODO
** To get just working stage of merge plugin, we need to find a way to correctly add new elements from slave model to master model.

** Correctly add elemets into model (find some decorator or factory, should it be somewhere/ask on forums)
**      - solve duplicit ID (generation of new IDs)
**      - do merge pairs dynamically
**                      - reformat merge pair code if needed
** Refactor code to polymorphich recursive method of copying (no need to different methods for nested elements and folders...)
** Get rid of global masterfolder
** Fix doxumentaion into logHTML
** Reformat code (use polymorfism, no need for separate methods for each kind of folders)
** Rework the documentation to something more usefull (method descriptions and prerequisities)
** Refactor logger to static singleton 
** Refactor code to single usage master/slave or master/slave
*/


package com.archimatetool.merge;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.PopupDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.archimatetool.editor.model.IEditorModelManager;
import com.archimatetool.model.FolderType;
import com.archimatetool.model.IArchimateFactory;
import com.archimatetool.model.IFolder;
import com.archimatetool.model.IIdentifier;
import com.archimatetool.model.IProperty;
import com.archimatetool.model.impl.ArchimateConcept;
import com.archimatetool.model.impl.ArchimateDiagramModel;
import com.archimatetool.model.impl.ArchimateModel;
import com.archimatetool.model.impl.ArchimateRelationship;
import com.archimatetool.model.impl.DiagramModelComponent;
import com.archimatetool.model.impl.DiagramModelConnection;

public class MergeModels implements IHandler {
	//the LogHTML class used to write the log file
	private LogHTML logHTML;
	//used when duplicate elements are present and need to be merged
	private MergeElements mergeElem;
	//the master and slave archimate models being merged
	private ArchimateModel slave, master;
	//an array of all of the duplicate elements between the two models
	private ArrayList<MergePairs> duplicates;
	//an array of all of the merged with exception pairs
	private ArrayList<MergePairs> exceptions;
	//a hashmap to store all the elements of the master models current folder
	private HashMap<String, ArchimateConcept> masterMap;
	//a hashmap to store the id's of the merged pairs
	private HashMap<String, Boolean> merges;
	//the current folder type
	private FolderType type;
	//stores the statistics of merges
	int successful, exception, instances; 
	// Neme prefix of newly generated folder for merge
	String GENERATED_FOLDER_NAME = "@Merge_";
	
	@Override
	public void addHandlerListener(IHandlerListener handlerListener) {
	}

	@Override
	public void dispose() {
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		//Initializing the popup dialog warning the user that the process is working
		PopupDialog pd = new PopupDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), PopupDialog.INFOPOPUP_SHELLSTYLE, true, false, false, false, false, "Merging Models", "\n\n\n\t\t\tPlease wait, process is working\t\t\t\n\n\n\n");
		//initializing variables
		mergeElem = new MergeElements();
		duplicates = new ArrayList<MergePairs>(); 
		masterMap = new HashMap<String, ArchimateConcept>();
		merges = new HashMap<String, Boolean>();
		exceptions = new ArrayList<MergePairs>();
		successful = 0; 
		exception = 0; 
		instances = 0; 
		
		//get the selections of the current tree
		ISelection sel = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getSelection();
		
		//check that the selections are not empty 
		if(sel.isEmpty()){
			return false;
		}
		//An array of the current selections
		TreePath[] tp = ((TreeSelection) sel).getPaths();
		
		//checking that there are two selections
		if(tp.length != 2) return false;   
		
		//create a new wizard with the names of the elements 
		MasterElement_Model me = new MasterElement_Model(((ArchimateModel) tp[0].getFirstSegment()).getName(), ((ArchimateModel) tp[1].getFirstSegment()).getName());
		
		//create a new wizard dialog 
		WizardDialog wd = new WizardDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), me){
			//this method sets the size of the wizard
			@Override
			protected void configureShell(Shell newShell){
				super.configureShell(newShell);
				newShell.setSize(496, 222);
			}
		};
		if(wd.open() == Window.CANCEL){
			return null;
		}
		
		//opening the popup dialog before the work begins
		pd.open();
		
		//the int representing the users master selection
		int check = me.getSelection();
		
		if(check == 1){
			//a will store the first model 
			master = (ArchimateModel) tp[0].getFirstSegment();
			//b stores the second model 
			slave = (ArchimateModel) tp[1].getFirstSegment();
		}
		else{
			//a will store the first model 
			slave = (ArchimateModel) tp[0].getFirstSegment();
			//b stores the second model 
			master = (ArchimateModel) tp[1].getFirstSegment();
		}
		
		//starting the log file
		logHTML = new LogHTML();
		logHTML.makeFile(master.getFile());
		logHTML.start(master.getName(), slave.getName());
		
		//for each folder, copy over the information
		type = FolderType.STRATEGY;
		copyFolder(slave.getFolder(FolderType.STRATEGY), master.getFolder(FolderType.STRATEGY));
		type = FolderType.BUSINESS;
		copyFolder(slave.getFolder(FolderType.BUSINESS), master.getFolder(FolderType.BUSINESS));
		type = FolderType.APPLICATION;
		copyFolder(slave.getFolder(FolderType.APPLICATION), master.getFolder(FolderType.APPLICATION));
		type = FolderType.TECHNOLOGY;
		copyFolder(slave.getFolder(FolderType.TECHNOLOGY), master.getFolder(FolderType.TECHNOLOGY));
		type = FolderType.MOTIVATION;
		copyFolder(slave.getFolder(FolderType.MOTIVATION), master.getFolder(FolderType.MOTIVATION));
		type = FolderType.IMPLEMENTATION_MIGRATION;
		copyFolder(slave.getFolder(FolderType.IMPLEMENTATION_MIGRATION), master.getFolder(FolderType.IMPLEMENTATION_MIGRATION));
		type = FolderType.OTHER;
		copyFolder(slave.getFolder(FolderType.OTHER), master.getFolder(FolderType.OTHER));
		//copying the relations folder using its special method
		type = FolderType.RELATIONS;
		copyFolderRelations(slave.getFolder(FolderType.RELATIONS), master.getFolder(FolderType.RELATIONS));
		//copying the diagrams folder using its special method
		IFolder mergeFolder = IArchimateFactory.eINSTANCE.createFolder();
		mergeFolder.setName(GENERATED_FOLDER_NAME);
		mergeFolder.setId(master.getIDAdapter().getNewID() + "GF");
		master.getFolder(FolderType.DIAGRAMS).getFolders().add(mergeFolder);
		copyFolderDiagrams(slave.getFolder(FolderType.DIAGRAMS), master.getFolder(FolderType.DIAGRAMS),mergeFolder); 	
		
		
		//getting the main model folder
		try{
			//save the model
			IEditorModelManager.INSTANCE.saveModel(master);
		}
		catch (IOException ioe){ //need the catch statement since saveModel() contains an IO operation
			System.out.println("IOException error");
		}
		
		//close the model 
		try{
			IEditorModelManager.INSTANCE.closeModel(slave);
		}
		catch(IOException ioe){
			System.out.println("Error closing the model");
		}
		
		//finishing the process of merging
		finish();
		//finishing the log file
		logHTML.finish(successful, exception, instances);
		//closing the popup menu, the process is completed
		pd.close();
		return null;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * 	No check needs to be performed on the relationships, so simply just copy every relation from slave to master. 
	 * @param slaveFolder the Relations folder of the slave element
	 * @param b the master element
	 * @return the updated master model
	 * <!-- end-user-doc -->
	 */
	private void copyFolderRelations(IFolder slaveFolder, IFolder masterFolder){
		
		/*IFolder mergeFolder = IArchimateFactory.eINSTANCE.createFolder();
		mergeFolder.setName(GENERATED_FOLDER_NAME);
		mergeFolder.setId(master.getIDAdapter().getNewID() + "GF");*/
		
		HashMap<String,EObject> masterFolderElements = new HashMap<>();
		for ( EObject masterElem : masterFolder.getElements()){
			if (masterElem instanceof IIdentifier){
				masterFolderElements.put(((IIdentifier) masterElem).getId(), masterElem);
			}
		}
		//start the relations section of the log file
		logHTML.startFolder("RELATIONS");
		//loop through all the elements not in a nested folder
		for(Object e : slaveFolder.getElements().toArray()){
			if(e instanceof ArchimateRelationship && !masterFolderElements.containsKey(((ArchimateRelationship) e).getId())){
				//add the elements to the Relations folder of b
				masterFolder.getElements().add((EObject) e);
				//write this relation to the log file
				if(!(merges.isEmpty())){
						logHTML.writeElements((ArchimateConcept) e, "New Instance Created" );
				}
			}
		}
		logHTML.finishFolder();
		//loop through all the nested folders of the Relations folder
		for(Object f : slaveFolder.getFolders().toArray()){
			if(f instanceof IFolder){
				//add the folders to the Relations folder of b
				masterFolder.getFolders().add((IFolder) f);
				//the folders were copied, but we want to write to the log file the elements in these folders
				writeRelations((IFolder)f);
			}
		}
		
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * Write the elements of the nested folders of the Relations folder to the log file. Recurses through infinite folder depths. 
	 * @param f the nested folder
	 * <!-- end-user-doc -->
	 */
	private void writeRelations(IFolder f){
		//start the nested folder
		logHTML.startNested(f.getName());
		//loop through each element in this folder
		for(Object e : f.getElements().toArray()){
			if(e instanceof ArchimateRelationship){
				//write this relationship to the log file
				if(!(merges.containsKey(((ArchimateRelationship) e).getSource().getId()) || merges.containsKey(((ArchimateRelationship) e).getTarget().getId()))){
					logHTML.writeElements((ArchimateConcept) e, "New Instance Created" );
				}
			}
		}
		logHTML.finishFolder();
		//loop through each folder in this folder
		for(Object o : f.getFolders().toArray()){
			if(o instanceof IFolder){
				//recurse with each folder
				writeRelations((IFolder)o);
			}
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * 	No check needs to be performed on the diagrams, so simply just copy every diagram from slave to master. 
	 * @param slaveFolder the Diagrams folder of the slave element
	 * @param masterModel the master element
	 * @return the updated master model
	 * <!-- end-user-doc -->
	 */
	private void copyFolderDiagrams(IFolder slaveFolder, IFolder masterFolder,IFolder mergeFolder){
		
		//loop through each of the folders in this folder
		HashMap<String,IFolder> masterDiagramFolders = new HashMap<>();
		for(IFolder folder : masterFolder.getFolders() ){
			masterDiagramFolders.put(folder.getId(), folder);
		}
		
		
		// copy folder structure of slave to master
		for(IFolder slaveSubFolder : slaveFolder.getFolders()){
			if(slaveSubFolder instanceof IFolder){
				//add the folders to the Diagram folder of b
				if(! masterDiagramFolders.containsKey(( slaveSubFolder).getId())) {
					//System.out.println("Master folder " + masterFolder.getName() + " does not contain subfolder " + slaveSubFolder.getName() + " ADDING");
					masterFolder.getFolders().add(slaveSubFolder);
				}else{
					//System.out.println("Master folder " + masterFolder.getName() + " does contain subfolder " + slaveSubFolder.getName() + " RECURSING");
					copyFolderDiagrams(slaveSubFolder, masterDiagramFolders.get(slaveSubFolder.getId()),mergeFolder);
				}
				//need to write the elements in these folders to the log file
				writeDiagrams(mergeFolder);
			}
		}
		
		//starting the Diagrams section of the log file
		logHTML.startFolder("DIAGRAMS");
		//looping through each element in the folder
		HashMap<String,ArchimateDiagramModel> masterDiagrams = new HashMap<String, ArchimateDiagramModel>();
		for( EObject elem : masterFolder.getElements()){
			if( elem instanceof ArchimateDiagramModel){
				masterDiagrams.put(((ArchimateDiagramModel) elem).getId(), ((ArchimateDiagramModel) elem));
			}
		}
		
		for(Object slaveElem : slaveFolder.getElements().toArray()){
			if(slaveElem instanceof ArchimateDiagramModel){
				//TODO pridat ze ak existuje treba zmenit ID a pridat hash kontrolu elementov vnutry a este zmenit nazov
				//change duplicit ID
				if(masterDiagrams.containsKey(((ArchimateDiagramModel) slaveElem).getId())){
					((ArchimateDiagramModel) slaveElem).setId(master.getIDAdapter().getNewID());
					((ArchimateDiagramModel) slaveElem).setName(((ArchimateDiagramModel) slaveElem).getName() + " slave copy");
				}
				//TODOsmazat komenty
				AddDiagramToMasterFolder(mergeFolder,(ArchimateDiagramModel) slaveElem);
				logHTML.writeElements((ArchimateDiagramModel) slaveElem, "Diagram Copied");
			}
		}
		logHTML.finishFolder();
		
	}
	
	private void AddDiagramToMasterFolder(IFolder masterFolder, ArchimateDiagramModel diagram) {
		//System.out.println("Adding diagram '" + diagram.getName() + "' to folder '" + masterFolder.getName() + "'");
		
		TreeIterator<EObject> diagramIterator = diagram.eAllContents();
		
		while(diagramIterator.hasNext()){
			EObject diagObj = diagramIterator.next();
			if ( diagObj instanceof DiagramModelConnection || diagObj instanceof DiagramModelComponent){
				//before addig new diagram to model we must upadate diagram object id so it would not be conflicting with existing diagram IDs
				String newId = master.getIDAdapter().getNewID();
				((IIdentifier) diagObj).setId(newId);
			}
		}
		
		/*for( IDiagramModelComponent child : diagram.getChildren()){
			String newId = master.getIDAdapter().getNewID();
			System.out.println("Changing id of child: " + child.toString());
			System.out.println("New id is: " + newId);
			child.setId(newId);
			//DiagramModelUtils.findDiagram
			//DiagramModelUtils.findDiagramModelComponentsForArchimateConcept(diagram, (IArchimateConcept) child);
		}*/
		masterFolder.getElements().add(diagram);
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * Write the elements of the nested folders of the Diagrams folder to the log file. Recurses through infinite folder depths. 
	 * @param f the nested folder
	 * <!-- end-user-doc -->
	 */
	private void writeDiagrams(IFolder f){
		//starting the nested folder's section in the log file
		logHTML.startNested(f.getName());
		//loop through each element in this folder
		for(Object e : f.getElements().toArray()){
			if(e instanceof ArchimateDiagramModel){
				//write the diagram to the log folder
				logHTML.writeElements((ArchimateDiagramModel) e, "Diagram Copied" );
			}
		}
		logHTML.finishFolder();
		//loop through the folders of this folder
		for(Object o : f.getFolders().toArray()){
			if(o instanceof IFolder){
				//recurse for each nested folder
				writeDiagrams((IFolder)o);
			}
		}
	}
	
	
	
	/**
	 * <!-- begin-user-doc -->
	 * The elements not in the relations or diagrams folder need to be checked against the elements of the master model. 
	 * This method checks each slave element against the master elements and then adds or removes the elements accordingly.
	 * @param slaveFolder the folder from the slave model being added to the master
	 * @param masterModel the master model
	 * @return the updated master model
	 * <!-- end-user-doc -->
	 */
	private void copyFolder(IFolder slaveFolder, IFolder masterFolder){
		//store all of the elements in the folder into an array
		EObject[] slaveIter = (EObject[]) slaveFolder.getElements().toArray();
		
		//store all of the folders in the folder into an array
		EObject[] slaveFolderIter = (EObject[]) slaveFolder.getFolders().toArray();
		
		//TODO increase speed duplicit
		//making the HashMap for all of the elements in B, but only if B has elements
		if((slaveIter.length != 0 || slaveFolderIter.length != 0)){
			//clear the hashMap before hand
			masterMap.clear();
			//method that makes the hash, stored in a global variable
			initializeMasterMap(masterFolder);
		}
		
		//loop through each of the folders in this folder
		HashMap<String,ArchimateConcept> slaveFolderElements = getAllElements(slaveFolder);
		
		//starting this folder's section of the log file
		logHTML.startFolder(type.toString().toUpperCase());
		IFolder mergeFolder = IArchimateFactory.eINSTANCE.createFolder();
		mergeFolder.setName(GENERATED_FOLDER_NAME);
		mergeFolder.setId(master.getIDAdapter().getNewID() + "GF");
		//if this folder has elements
		if(slaveIter.length != 0){
						
			//for each element in the array, copy it over to the other model if it doesn't already exist 
			for(String key : slaveFolderElements.keySet()){
					ArchimateConcept less = slaveFolderElements.get(key);
					//if the master model already contains this element
					if(masterMap.containsKey(key)){	
						ArchimateConcept mast = masterMap.get(key);
						//create a new pair
						MergePairs mp = new MergePairs(mast, less);
						//add the pair to the array
						duplicates.add(mp);
						merges.put(mast.getId(), true);
						//write this to the log file
						if(checkProperties(mast, less) && mast.getName().equals(less.getName())){
							//The elements are the same, so it will be a successful merge
							logHTML.writeElements(less, "<td style='background-color: #a7ff8b'>Successfully Merged</td>");
							successful++;
						}
						else{
							//the elements are different, merge with exceptions
							logHTML.writeElements(less, "<td style='background-color: #f9e77d'>Merged with Exceptions</td>");
							//TODO null pointer
							mp = new MergePairs(mast, less, mast.getProperties(), less.getProperties());
							exceptions.add(mp);
							exception++;
						}
					}
					//the master model did not contain this element
					else{
						//add the element to the appropriate folder
						mergeFolder.getElements().add(less);
						//write this to the log file
						logHTML.writeElements(less, "<td style='background-color: #7db1f9'>New Instance Created</td>");
						instances++; 
					}
			}
			masterFolder.getFolders().add(mergeFolder);
		}
		
		logHTML.finishFolder();
	}
	
	
	private HashMap<String, ArchimateConcept> getAllElements(IFolder folder) {
		HashMap<String, ArchimateConcept> subElements = new HashMap<String, ArchimateConcept>();
		//loop through each element in this folder
		for(EObject concept : folder.getElements()){
			if(concept instanceof ArchimateConcept){
				//add the element to the HashMapo
				subElements.put(((ArchimateConcept) concept).getId(), (ArchimateConcept) concept);
			}
		}
		//loop through each folder in this folder
		//TODO no need for new merginging
		for(Object subFolder : folder.getFolders().toArray()){
			if(subFolder instanceof IFolder){
				//recurse through each folder
				subElements.putAll(getAllElements((IFolder) subFolder));
			}
		}
		return subElements;
	}
	
	private boolean checkProperties(ArchimateConcept a, ArchimateConcept b){
		for(IProperty ip : a.getProperties()){
			for(IProperty p : b.getProperties()){
				if(!(ip.getValue().equals(p.getValue()))){
					return false;
				}
			}
		}
		return true; 
	}
	

	/**
	 * <!-- begin-user-doc -->
	 *  Creates the HashMap for each folder in B. This allows for efficient searching of the elements of the master element
	 *  @param f the folder whose elements are being added to the HashMap
	 * <!-- end-user-doc -->
	 */
	private void initializeMasterMap(IFolder f){
		//loop through each element in this folder
		for(Object e : f.getElements().toArray()){
			if(e instanceof ArchimateConcept){
				//add the element to the HashMapo
				masterMap.put(((ArchimateConcept) e).getId(), (ArchimateConcept) e);
			}
		}
		
		//loop through each folder in this folder
		//TODO no need for new merginging
		for(Object o : f.getFolders().toArray()){
			if(o instanceof IFolder){
				//recurse through each folder
				initializeMasterMap((IFolder) o);
			}
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * Finishes the process by merging all of the duplicate elements
	 * <!-- end-user-doc -->
	 */
	private void finish(){
		//sets the log file
		mergeElem.setLH(logHTML);
		//starts the merge section of the log file
		logHTML.startMerge();
		//loops through all of the duplicates
		for(MergePairs mp : duplicates){
			//merge the duplicates
			mergeElem.execute_from_master(mp.master, mp.slave, master);
		}
		logHTML.startMergeExceptions();
		for(MergePairs mp : exceptions){
			writeExceptions(mp);
		}
	}
	
	/**
	 * Writes all of the "Merged with Exceptions" exceptions to the log file
	 * @param mp the pair of elements that were merged with an exception
	 */
	private void writeExceptions(MergePairs mp){
		ArchimateConcept slave = mp.slave; 
		ArchimateConcept master = mp.master; 
		
		logHTML.startException(master, slave);
		
		if(!(master.getName().equals(slave.getName()))){
			logHTML.startName(master.getName(), slave.getName());
		}
		if(mp.master_p != null && mp.slave_p != null){
			if(!(mp.master_p.toString().equals(mp.slave_p.toString()))){
				logHTML.startProperty();
				for(Object p : mp.master_p){
					for(Object q : mp.slave_p){
						if(((IProperty) p).getKey().equals(((IProperty) q).getKey())){
							if(!(((IProperty) p).getValue().equals(((IProperty) q).getValue()))){
								logHTML.addProperty(((IProperty) p).getKey(), ((IProperty) p).getValue(), ((IProperty) q).getValue());
							}
						}
					}
				}
			}
		}
	}
	
	@Override
	public boolean isEnabled() {
		ISelection sel = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getSelection();
		
		//sel returns an array of everything that is selected
		//just check that the array is just two archimate models (do this by checking class type)
		if(sel.isEmpty()){
			return false;
		}
		TreePath[] tp = ((TreeSelection) sel).getPaths();
		
		if(tp.length != 2) return false;   
		
		ArchimateModel a = (ArchimateModel) tp[0].getFirstSegment();
		ArchimateModel b = (ArchimateModel) tp[1].getFirstSegment();
		
		if(a.getFile() == null) return false; 
		if(b.getFile() == null) return false; 
		
		if(a.getFile().getAbsolutePath().equals(b.getFile().getAbsolutePath())){
			 return false; 
		}	

		return true;
	}

	@Override
	public boolean isHandled() {
		
		return true;
	}

	@Override
	public void removeHandlerListener(IHandlerListener handlerListener) {
		
	}

}
